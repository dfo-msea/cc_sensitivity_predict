<<<<<<< HEAD
# Projecting groundfish responses to future changes in temperature and oxygen

__Main author:__  Patrick Thompson  
__Contributors:__ Jessica Nephin, Amber Holdsworth, Sarah Davies, Ashley Park, Devin Lyons, Chris Rooper, Karen Hunter, Emily Rubidge, Angelica Pena, Jim Christian
=======
# Groundfish biodiversity change in northeastern Pacific waters under projected warming and deoxygenation

__Main author:__  Patrick Thompson  
__Contributors:__ Jessica Nephin, Sarah Davies, Ashley Park, Devin Lyons, Chris Rooper, Angelica Pena, Jim Christian, Karen Hunter, Emily Rubidge, Amber Holdsworth,
>>>>>>> c7bc8ddee019657d216a535149203373c12aeca4
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: patrick.thompson@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Develop projections for how groundfish will respond to climate change

## Summary
Combine NOAA and DFO survey data to look at species relationships to temperature, oxygen, and depth across large latitudinal gradient and use NEP36 and BCCM regional climate models to project responses into future.

## Status
Submitted

## Contents
R code for loading data and performing analysis. 

## Methods

## Requirements

## Caveats

