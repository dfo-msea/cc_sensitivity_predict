#Code by Patrick Thompson
#patrick.thompson@dfo-mpo.gc.ca
#June 2021

#load packages####
library(tidyverse)
library(tidyverse)
library(viridis)
library(sf)
library(cowplot)
library(scales)
library(PupillometryR)
library(patchwork)
library(ModelMetrics)
library(sdmTMB)

#functions####
source("./code/functions/plt_plot_theme.R")
theme_set(plt_theme)

reverselog_trans <- function(base = exp(1)) {
  trans <- function(x) -log(x, base)
  inv <- function(x) base^(-x)
  trans_new(paste0("reverselog-", format(base)), trans, inv,
            log_breaks(base = base),
            domain = c(1e-100, Inf))
}

tjur <- function(y, pred) {
  categories <- sort(unique(y))
  m1 <- mean(pred[which(y == categories[1])], na.rm = TRUE)
  m2 <- mean(pred[which(y == categories[2])], na.rm = TRUE)
  abs(m2 - m1)
}

#load basemap####
coastline <- read_sf("../../datasets/Coastline_low_res/BC-PacNW_BCAlbers.shp")

#load survey data####
combined_data <- read_rds(file = "./data/combined_surveys_mod_O2.rds")
combined_data <- combined_data %>%
  filter(!is.na(O2)) %>%
  dplyr::select(-X, -Y)

combined_data$Survey <- combined_data$Survey2

combined_data$common_name[combined_data$common_name == "rougheye/blackspotted rockfish complex"] <- "rougheye rockfish complex"
combined_data$common_name[combined_data$common_name == "blackfin sculpin"] <- "darkfin sculpin"

exclude <- c("blackfin sculpin","black eelpout","black hagfish","black sculpin","buffalo sculpin", "copper rockfish","pacific sand lance", "shiner perch", "shortbelly rockfish")

survey_wide <- combined_data %>%
  filter(!(common_name %in% exclude)) %>%
  mutate(common_name = str_to_title(common_name)) %>%
  filter(!is.na(depth_m)) %>%
  dplyr::group_by(common_name, depth_m, year, latitude, longitude, temperature_C, O2, O2_ln, Survey, salinity, O2_obs) %>%
  summarise(cpue_kg_per_ha_der = sum(cpue_kg_per_ha_der)) %>%
  spread(key = common_name, value = cpue_kg_per_ha_der, fill = 0) %>%
  ungroup()

survey_sf <- st_as_sf(survey_wide %>% ungroup(), coords = c("longitude", "latitude"))
survey_sf <- st_set_crs(survey_sf, "+proj=longlat +datum=WGS84 +ellps=WGS84")
survey_sf <- st_transform(survey_sf,  "EPSG:3005")


#prepare env variables
survey_sf <- survey_sf %>%
  mutate(depth_ln = log(depth_m)) %>%
  mutate(O2_ln_stnd = (O2_ln - mean(O2_ln))/sd(O2_ln),
         depth_ln_stnd = (depth_ln - mean(depth_ln))/sd(depth_ln),
         temp_stnd = (temperature_C - mean(temperature_C))/sd(temperature_C))


Y <- survey_sf %>% dplyr::select(`Aleutian Skate`:`Yellowtail Rockfish`)
st_geometry(Y) <- NULL
Y[Y>0] <- 1

#prepare prediction grid
load("./data/env_grid.RData")
env_grid.df <- env_grid.df %>% 
  mutate(O2_ln = log(O2), depth_ln = log(depth_m)) %>% 
  mutate(temp_stnd = (temperature - mean(survey_sf$temperature_C))/sd(survey_sf$temperature_C),
         O2_ln_stnd = (O2_ln - mean(survey_sf$O2_ln))/sd(survey_sf$O2_ln),
         depth_ln_stnd = (depth_ln - mean(survey_sf$depth_ln))/sd(survey_sf$depth_ln)) %>% 
  mutate(Survey = "DFO_Pacific")

env_grid_sf <- st_as_sf(env_grid.df, coords = c("x", 'y'))
env_grid_sf <- st_set_crs(env_grid_sf, value = "EPSG:3005")

env_grid.df$X <- env_grid.df$x/1000
env_grid.df$Y <- env_grid.df$y/1000
env_grid.df$temperature_C <- env_grid.df$temperature

#crop coastline
coastline <- st_crop(coastline, env_grid_sf)

temp_seq <- seq(floor(min(survey_sf$temperature_C)), ceiling(max(survey_sf$temperature_C)),length = 30)
temp_labels <- rowMeans(cbind(temp_seq[-length(temp_seq)], temp_seq[-1]))
depth_seq <- seq(min(survey_sf$depth_ln), max(survey_sf$depth_ln), length = 30)
depth_labels <- exp(rowMeans(cbind(depth_seq[-length(depth_seq)], depth_seq[-1])))
O2_seq <- seq(min(survey_sf$O2_ln), max(survey_sf$O2_ln), length = 30)
O2_labels <- exp(rowMeans(cbind(O2_seq[-length(O2_seq)], O2_seq[-1])))

gridded_obs <- survey_sf %>% 
  st_set_geometry(value = NULL) %>% 
  dplyr::select(depth_m, depth_ln, temperature_C, O2_ln) %>% 
  mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
  mutate(depth_bin = as.numeric(as.character((cut(depth_ln, breaks = depth_seq, right = FALSE, labels = depth_labels))))) %>% 
  mutate(O2_bin = as.numeric(as.character(cut(O2_ln, breaks = O2_seq, right = FALSE, labels = O2_labels))))

depth_temp_predict.df <- gridded_obs %>% 
  group_by(temp_bin, depth_bin) %>% 
  summarise(O2_ln = median(O2_ln)) %>% 
  ungroup() %>% 
  mutate(temperature_C = temp_bin, depth = depth_bin, depth_ln = log(depth_bin), Survey = "DFO_Pacific")

depth_temp_predict.df <- depth_temp_predict.df[complete.cases(depth_temp_predict.df),]
depth_temp_predict.df$X <- NA
depth_temp_predict.df$Y <- NA

O2_temp_predict.df <- gridded_obs %>% 
  group_by(temp_bin, O2_bin) %>% 
  summarise(depth_ln = median(depth_ln)) %>% 
  mutate(O2 = O2_bin, O2_ln = log(O2_bin), temperature_C = temp_bin, Survey = "DFO_Pacific")

O2_temp_predict.df <- O2_temp_predict.df[complete.cases(O2_temp_predict.df),]
O2_temp_predict.df$X <- NA
O2_temp_predict.df$Y <- NA

ggplot(depth_temp_predict.df, aes(x = temperature_C, y = depth, fill = O2_ln))+
  geom_tile()+
  scale_y_continuous(trans = reverselog_trans(10))+
  scale_fill_viridis()

ggplot(O2_temp_predict.df, aes(x = temperature_C, y = O2_bin, fill = depth_ln))+
  geom_tile()+
  scale_y_log10()+
  scale_fill_viridis()

survey_region <- survey_sf %>%
  st_crop(env_grid_sf)

#make single species prediction####
species <- "Pacific Cod"
map_future_sp <- function(species){
  survey_sf$presence <- unlist(Y[,species])
  
  present_all <- survey_sf %>% 
    group_by(Survey) %>% 
    summarise(n_pres = sum(presence))
  if(present_all$n_pres[present_all$Survey == "NOAA.Bering"] == 0){
    survey_sf <- survey_sf %>% 
      mutate(Survey = as.character(Survey)) %>% 
      mutate(Survey = factor(ifelse(Survey == "NOAA.Bering", "NOAA_Alaska", Survey)))
  }
  
  survey_region <- survey_sf %>%
    st_crop(env_grid_sf)
  
  survey_present <- survey_region %>%
    filter(presence == 1)
  
  temp_seq <- floor(min(survey_sf$temperature_C)): ceiling(max(survey_sf$temperature_C))
  temp_labels <- rowMeans(cbind(temp_seq[-length(temp_seq)], temp_seq[-1]))
  depth_seq <- seq(min(survey_sf$depth_ln), max(survey_sf$depth_ln), length = 15)
  depth_labels <- exp(rowMeans(cbind(depth_seq[-length(depth_seq)], depth_seq[-1])))
  O2_seq <- seq(min(survey_sf$O2_ln), max(survey_sf$O2_ln), length = 15)
  O2_labels <- exp(rowMeans(cbind(O2_seq[-length(O2_seq)], O2_seq[-1])))
  
  temp_depth_all <- survey_sf %>%
    st_set_geometry(value = NULL) %>%
    dplyr::select(depth_m, depth_ln, temperature_C, presence) %>%
    mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
    mutate(depth_bin = as.numeric(as.character((cut(depth_ln, breaks = depth_seq, right = FALSE, labels = depth_labels))))) %>%
    group_by(temp_bin, depth_bin) %>%
    summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), depth = mean(depth_m)) %>%
    arrange(desc(prop))
  
  temp_O2_all <- survey_sf %>%
    st_set_geometry(value = NULL) %>%
    dplyr::select(O2, O2_ln,temperature_C, presence) %>%
    mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
    mutate(O2_bin = as.numeric(as.character(cut(O2_ln, breaks = O2_seq, right = FALSE, labels = O2_labels)))) %>%
    group_by(temp_bin, O2_bin) %>%
    summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), O2 = mean(O2)) %>%
    arrange(desc(prop))
  
  temp_depth_DFO <- survey_region %>%
    st_set_geometry(value = NULL) %>%
    dplyr::select(depth_m, depth_ln, temperature_C, presence) %>%
    mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
    mutate(depth_bin = as.numeric(as.character((cut(depth_ln, breaks = depth_seq, right = FALSE, labels = depth_labels))))) %>%
    group_by(temp_bin, depth_bin) %>%
    summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), depth = mean(depth_m)) %>%
    arrange(desc(prop))
  
  temp_O2_DFO <- survey_region %>%
    st_set_geometry(value = NULL) %>%
    dplyr::select(O2, O2_ln,temperature_C, presence) %>%
    mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
    mutate(O2_bin = as.numeric(as.character(cut(O2_ln, breaks = O2_seq, right = FALSE, labels = O2_labels)))) %>%
    group_by(temp_bin, O2_bin) %>%
    summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), O2 = mean(O2)) %>%
    arrange(desc(prop))
  
  depth_select <-  exp(mean(survey_region$depth_ln[survey_region$presence == 1]))
  temp_select <- mean(survey_region$temperature_C[survey_region$presence == 1])
  O2_select <- exp(mean(survey_region$O2_ln[survey_region$presence == 1]))
  
  data_m <- survey_sf %>% st_drop_geometry() %>% select(presence, depth_ln, O2_ln, temperature_C, Survey, year)
  data_m$Survey <- factor(data_m$Survey)
  data_m$presence <- unlist(data_m$presence)
  
  XY <- data.frame(st_coordinates(survey_sf))/1000
  survey_sf$X <- XY$X
  survey_sf$Y <- XY$Y
  mesh <- make_mesh(data = XY, xy_cols = c("X", "Y"), cutoff = 15)
  #m <- glm(presence ~ poly(temp_stnd, 2, raw = TRUE) + poly(O2_ln_stnd, 2, raw = TRUE) + poly(depth_ln_stnd, 2, raw = TRUE) + Survey, data = data_m, family = binomial(link = "logit"))
  #summary(m)
  m <- sdmTMB(presence ~ poly(temperature_C, 2, raw = TRUE) + breakpt(O2_ln) + poly(depth_ln, 2, raw = TRUE) + Survey, data = data_m, family = binomial(link = "logit"), mesh = mesh, spatial = "off", spatiotemporal = "off")
  if(max(m$gradients)>0.01){
    m <- sdmTMB::run_extra_optimization(m, nlminb_loops = 1L, newton_loops = 1L)
  }
  coefs_full <- tidy(m)
  
  pos_slope <- coefs_full$estimate[coefs_full$term == "O2_ln-slope"] > 0
  break_in_range <- exp(coefs_full$estimate[coefs_full$term == "O2_ln-breakpt"]) > 0 & exp(coefs_full$estimate[coefs_full$term == "O2_ln-breakpt"]) < 10
  std.error_break_ok <- !is.nan(coefs_full$std.error[coefs_full$term == "O2_ln-breakpt"]) & coefs_full$std.error[coefs_full$term == "O2_ln-breakpt"] < log(10)/1.96
  std.error_slope_ok <- !is.nan(coefs_full$std.error[coefs_full$term == "O2_ln-slope"])
  good_mod <- pos_slope & break_in_range & std.error_break_ok & std.error_slope_ok
  if(!good_mod){
    m <- sdmTMB(presence ~ poly(temperature_C, 2, raw = TRUE) + poly(depth_ln, 2, raw = TRUE) + Survey, data = data_m, family = binomial(link = "logit"), mesh = mesh, spatial = "off", spatiotemporal = "off")
    if(max(m$gradients)>0.01){F
      m <- sdmTMB::run_extra_optimization(m, nlminb_loops = 1L, newton_loops = 1L)
    }
    coefs <- tidy(m)
  } else {
    coefs <- tidy(m)
  }   
  
  
  #m_forecast <- glm(presence ~ poly(temp_stnd, 2, raw = TRUE) + poly(O2_ln_stnd, 2, raw = TRUE) + poly(depth_ln_stnd, 2, raw = TRUE) + Survey, data = data_m %>% filter(year <2011), family = binomial(link = "logit"))
  XY <- data.frame(st_coordinates(survey_sf %>% filter(year < 2011)))/1000
  mesh <- make_mesh(data = XY, xy_cols = c("X", "Y"), cutoff = 15)
  
  if(good_mod){
    m_forecast <- sdmTMB(presence ~ poly(temperature_C, 2, raw = TRUE) + breakpt(O2_ln) + poly(depth_ln, 2, raw = TRUE) + Survey, data = data_m %>% filter(year <2011), family = binomial(link = "logit"), mesh = mesh, spatial = "off", spatiotemporal = "off")
    if(max(m_forecast$gradients)>0.01){
      m_forecast <- sdmTMB::run_extra_optimization(m_forecast, nlminb_loops = 1L, newton_loops = 1L)
    }
  } else {
    m_forecast <- sdmTMB(presence ~ poly(temperature_C, 2, raw = TRUE) + poly(depth_ln, 2, raw = TRUE) + Survey, data = data_m %>% filter(year <2011), family = binomial(link = "logit"), mesh = mesh, spatial = "off", spatiotemporal = "off")
    if(max(m_forecast$gradients)>0.01){
      m_forecast <- sdmTMB::run_extra_optimization(m_forecast, nlminb_loops = 1L, newton_loops = 1L)
    }
  }
  
  survey_region.df <- survey_region %>% st_drop_geometry() %>% select(presence, depth_ln, O2_ln, temperature_C, Survey, year)
  XY <- data.frame(st_coordinates(survey_region))/1000
  survey_region.df$X <- XY$X
  survey_region.df$Y <- XY$Y
  survey_region$X <- XY$X
  survey_region$Y <- XY$Y
  
  forecast <- plogis(predict(m_forecast, newdata = survey_region.df %>% filter(year > 2010))$est)
  pre_2011 <- plogis(predict(m_forecast, newdata = survey_region.df %>% filter(year < 2011))$est)
  
  forecast_predict_sp <- data.frame(TjurR2 = c(tjur(y = survey_region.df$presence[survey_region.df$year > 2010], pred = forecast),
                                               tjur(y = survey_region.df$presence[survey_region.df$year < 2011], pred = pre_2011)),
                                    AUC = c(auc(survey_region.df$presence[survey_region.df$year > 2010], forecast),
                                            auc(survey_region.df$presence[survey_region.df$year < 2011], pre_2011)),
                                    type = factor(c("forecast", "training"), levels = c("training", "forecast"), ordered =  TRUE),
                                    species = species)
  
  depth_temp_predict.df$predict <- plogis(predict(m, newdata = depth_temp_predict.df)$est)
  depth_temp_predict.df$species <- species
  O2_temp_predict.df$predict <- plogis(predict(m, O2_temp_predict.df)$est)
  O2_temp_predict.df$species <- species
  
  #estimate margins
  survey_sf$predict <- plogis(predict(m, st_drop_geometry(survey_sf))$est)
  
  max_min <- survey_sf %>% 
    st_set_geometry(value = NULL) %>%
    filter(presence == 1) %>% 
    summarise(depth_min = min(depth_m),
              depth_max = max(depth_m), 
              O2_min = min(O2),
              O2_max = max(O2),
              temp_min = min(temperature_C),
              temp_max = max(temperature_C))
  
  all_limits <- survey_sf %>% 
    st_set_geometry(value = NULL) %>%
    filter(predict>=0.1) %>% 
    filter(depth_m < max_min$depth_max+50, depth_m > max_min$depth_min-50) %>%
    filter(temperature_C < max_min$temp_max+1, depth_m > max_min$temp_min-1) %>%
    filter(O2 < max_min$O2_max+0.5, depth_m > max_min$O2_min-0.5) %>% 
    summarise(O2 = min(O2), temp = max(temperature_C), depth = max(depth_m)) %>% 
    mutate(depth = ifelse(depth>max(survey_region$depth_m), max(survey_region$depth_m), depth))
  
  if(good_mod){
  O2_breakpt <- coefs %>% filter(term == "O2_ln-breakpt") %>% pull(estimate)
  all_limits$O2_breakpt <- exp(O2_breakpt)
  } else {all_limits$O2_breakpt <- NA}
  
  diff_dist_sp <- survey_region %>% 
    filter(presence == 1) %>% 
    mutate(temp_diff = temperature_C - all_limits$temp, depth_diff = depth_m - all_limits$depth, O2_diff = O2 - all_limits$O2, O2_diff_break = O2 - all_limits$O2_breakpt) %>% 
    dplyr::select(temp_diff, depth_diff, O2_diff, O2_diff_break, temperature_C, depth_m, O2) %>% 
    mutate(Species = species)
  
  survey_region$predict <- plogis(predict(m, st_drop_geometry(survey_region))$est)
  
  predictions <- predict(m, env_grid.df, nsim = 500)
  hold.df <- data.frame(prediction = plogis(predict(m, env_grid.df)$est), 
                        lower_95 = plogis(apply(predictions, MARGIN = 1, FUN = function(x) quantile(x, probs = 0.025))),
                        upper_95 = plogis(apply(predictions, MARGIN = 1, FUN = function(x) quantile(x, probs = 0.975))))
  
  env_grid.df$prediction <- hold.df$prediction
  env_grid.df$lower_95 <- hold.df$lower_95
  env_grid.df$upper_95 <- hold.df$upper_95
  
  RCP_predictions.df <- env_grid.df %>% 
    ungroup() %>% 
    group_by(x, y, model) %>% 
    mutate(temperature_delta = temperature - temperature[scenario == "historical"],
           O2_delta = O2 - O2[scenario == "historical"],
           difference = prediction-prediction[scenario == "historical"], 
           hist_range = prediction[scenario == "historical"]>0.1) %>% 
    ungroup() %>% 
    group_by(x, y, scenario, model) %>% 
    mutate(range = prediction > 0.1) %>% 
    mutate(diff_in_range = ifelse(range == TRUE | hist_range == TRUE, difference, NA), 
           range_change = ifelse(hist_range == TRUE & range == TRUE, "retained", ifelse(hist_range == FALSE & range == TRUE, "gained", ifelse(hist_range == TRUE & range == FALSE, "lost", "not suitable")))) %>%  
    mutate(range_change = factor(range_change, levels = c("not suitable", "lost", "retained", "gained"), ordered = TRUE)) %>% 
    ungroup()
  
  coefs$species <- species
  
  RCP_predictions.df$species <-species
  return(list(RCP_predictions.df, diff_dist_sp, depth_temp_predict.df, O2_temp_predict.df, forecast_predict_sp, coefs))
}

hold <- map_future_sp("Big Skate")

predictive.df <- readRDS("./data/predictive_power.rds") %>% 
  filter(TjurR2.f > 0.2)

prediction_all.df <- data.frame()
margin_dist.df <- data.frame()
depth_temp_curves.df <- data.frame()
O2_temp_curves.df <- data.frame()
forecast_accuracy <- data.frame()
coefs.df <- data.frame()
for(i in predictive.df$species){
  print(i)
  hold <- tryCatch(map_future_sp(i), error = function(e) {print(i)})
  if(length(hold)>1){
    prediction_all.df <- rbind(prediction_all.df, hold[[1]])
    margin_dist.df <- rbind(margin_dist.df, hold[[2]])
    depth_temp_curves.df <- rbind(depth_temp_curves.df, hold[[3]])
    O2_temp_curves.df <- rbind(O2_temp_curves.df, hold[[4]])
    forecast_accuracy <- rbind(forecast_accuracy, hold[[5]])
    coefs.df <- rbind(coefs.df, hold[[6]])
  }
}

forecast_accuracy <- forecast_accuracy %>% 
  group_by(species) %>% 
  summarise(delta_AUC = AUC[type == "forecast"] - AUC[type == "training"],
            delta_TjurR2 = TjurR2[type == "forecast"] - TjurR2[type == "training"], 
            TjurR2_pre2011 = TjurR2[type == "training"],
            TjurR2_post2010 = TjurR2[type == "forecast"],
            AUC_pre2011 = AUC[type == "training"],
            AUC_post2010 = AUC[type == "forecast"])

species_select <- forecast_accuracy$species[forecast_accuracy$AUC_post2010 > 0.2]

prediction_all.df <- prediction_all.df %>% filter(species %in% species_select)
margin_dist.df <- margin_dist.df %>% filter(species %in% species_select)
depth_temp_curves.df <- depth_temp_curves.df %>% filter(species %in% species_select)
O2_temp_curves.df <- O2_temp_curves.df %>% filter(species %in% species_select)

prediction_all.df <- prediction_all.df %>%
  mutate(species = as.character(species)) %>%
  group_by(species, scenario) %>%
  mutate(median_diff_in_range = median(diff_in_range, na.rm = TRUE)) %>%
  left_join(predictive.df %>% select(-model)) %>% 
  ungroup() %>% 
  group_by(species) %>% 
  mutate(keep = sum(!is.na(diff_in_range))) %>% 
  filter(keep > 0) %>% 
  dplyr::select(-keep)

change_order <- prediction_all.df %>%
  ungroup() %>% 
  filter(scenario == "RCP.8.5", model == "BCCM") %>% 
  dplyr::select(species, median_diff_in_range) %>% 
  unique() %>% 
  arrange(median_diff_in_range)

prediction_all.df$species <- factor(prediction_all.df$species, levels = change_order$species, ordered = TRUE)

prediction_RCP8.5.df <- prediction_all.df %>% filter(scenario == "RCP 8.5")

change_order <- prediction_all.df %>% 
  filter(scenario == "RCP.8.5", model == "BCCM") %>% 
  dplyr::select(species, median_diff_in_range) %>% 
  unique() %>% 
  arrange(median_diff_in_range)

prediction_all.df <- prediction_all.df %>% filter(difference < 0.7) #remove values for Popeye that are unreasonably high due to model fitting issues.
#this is two cells in waters far shallower than their depth range where occurrence goes from 0 to nearly 1 under RCP 8.5

#margin figure####
margin_dist.df <- margin_dist.df %>% 
  left_join(change_order, by = c("Species" = "species")) %>% 
  mutate(Species = factor(Species, levels = change_order$species, ordered = TRUE))

region_obs <- survey_sf %>% st_crop(env_grid_sf) %>%
  st_set_geometry(value = NULL) %>% 
  gather(key = Species, value = abund, `Aleutian Skate`: `Yellowtail Rockfish`) %>% 
  group_by(Species) %>% 
  summarise(Obs = sum(abund>0)) %>% 
  arrange(Obs)

margin_dist.df <- margin_dist.df %>% filter(Species %in% region_obs$Species[region_obs$Obs>50])

#predicted change in species richness####
richness_df <- prediction_all.df %>%
  ungroup() %>%
  group_by(x, y, depth_m, O2, O2_delta, temperature, temperature_delta, scenario, model) %>%
  summarise(gains = sum(difference[difference>0]),
            losses = sum(difference[difference<0]),
            richness = sum(prediction)) %>% 
  ungroup() %>% 
  group_by(x, y, depth_m, model) %>% 
  mutate(rich_hist = richness[scenario == "historical"],
         rich_diff = gains + losses,
         mean_alpha = (richness + richness[scenario == "historical"])/2,
         gamma = richness[scenario == "historical"] + gains,
         shared = (richness[scenario == "historical"] + losses)) %>%
  mutate(beta = gamma - mean_alpha,
         turnover = gains - losses,
         Jaccard = (1-(shared/gamma)))


#faceted all species plots ####

temp_seq <- floor(min(survey_sf$temperature_C)): ceiling(max(survey_sf$temperature_C))
temp_labels <- rowMeans(cbind(temp_seq[-length(temp_seq)], temp_seq[-1]))
depth_seq <- seq(min(survey_sf$depth_ln), max(survey_sf$depth_ln), length = 15)
depth_labels <- exp(rowMeans(cbind(depth_seq[-length(depth_seq)], depth_seq[-1])))
O2_seq <- seq(min(survey_sf$O2_ln), max(survey_sf$O2_ln), length = 15)
O2_labels <- exp(rowMeans(cbind(O2_seq[-length(O2_seq)], O2_seq[-1])))

temp_O2_all <- survey_sf %>%
  st_set_geometry(value = NULL) %>%
  gather(key = species, value = presence, `Aleutian Skate`: `Yellowtail Rockfish`) %>% 
  mutate(presence = as.numeric(presence > 0)) %>% 
  dplyr::select(O2, O2_ln, temperature_C, presence, species) %>%
  mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
  mutate(O2_bin = as.numeric(as.character(cut(O2_ln, breaks = O2_seq, right = FALSE, labels = O2_labels)))) %>%
  group_by(temp_bin, O2_bin, species) %>%
  summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), O2 = mean(O2)) %>%
  arrange(desc(prop))

temp_O2_sub <- temp_O2_all %>% 
  filter(species %in% unique(O2_temp_curves.df$species))

temp_O2_region <- survey_region %>%
  st_set_geometry(value = NULL) %>%
  gather(key = species, value = presence, `Aleutian Skate`: `Yellowtail Rockfish`) %>% 
  mutate(presence = as.numeric(presence > 0)) %>% 
  dplyr::select(O2, O2_ln, temperature_C, presence, species) %>%
  mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
  mutate(O2_bin = as.numeric(as.character(cut(O2_ln, breaks = O2_seq, right = FALSE, labels = O2_labels)))) %>%
  group_by(temp_bin, O2_bin, species) %>%
  summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), O2 = mean(O2)) %>%
  arrange(desc(prop))

temp_O2_region_sub <- temp_O2_region %>% 
  filter(species %in% unique(O2_temp_curves.df$species))

RCP8.5_arrows <- prediction_RCP8.5.df %>% filter(hist_range == TRUE) %>% 
  group_by(species) %>% 
  summarise(temperature_delta = mean(temperature_delta), O2_delta = mean(O2_delta), temperature = mean(temperature), O2 = mean(O2), depth_m = mean(depth_m)) %>% 
  mutate(temp_initial = temperature - temperature_delta, O2_initial = O2 - O2_delta)


temp_depth_all <- survey_sf %>%
  st_set_geometry(value = NULL) %>%
  gather(key = species, value = presence, `Aleutian Skate`: `Yellowtail Rockfish`) %>% 
  mutate(presence = as.numeric(presence > 0)) %>% 
  dplyr::select(depth_m, depth_ln, temperature_C, presence, species) %>%
  mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
  mutate(depth_bin = as.numeric(as.character((cut(depth_ln, breaks = depth_seq, right = FALSE, labels = depth_labels))))) %>%
  group_by(temp_bin, depth_bin, species) %>%
  summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), depth = mean(depth_m)) %>%
  arrange(desc(prop))

temp_depth_sub <- temp_depth_all %>% 
  filter(species %in% unique(depth_temp_curves.df$species))

temp_depth_region <- survey_region %>%
  st_set_geometry(value = NULL) %>%
  gather(key = species, value = presence, `Aleutian Skate`: `Yellowtail Rockfish`) %>% 
  mutate(presence = as.numeric(presence > 0)) %>% 
  dplyr::select(depth_m, depth_ln, temperature_C, presence, species) %>%
  mutate(temp_bin = as.numeric(as.character((cut(temperature_C, breaks = temp_seq, right = FALSE, labels = temp_labels))))) %>%
  mutate(depth_bin = as.numeric(as.character((cut(depth_ln, breaks = depth_seq, right = FALSE, labels = depth_labels))))) %>%
  group_by(temp_bin, depth_bin, species) %>%
  summarise(count = sum(presence), prop = sum(presence)/n(), total_trawls = n(), temp = mean(temperature_C), depth = mean(depth_m)) %>%
  arrange(desc(prop))

temp_depth_region_sub <- temp_depth_region %>% 
  filter(species %in% unique(depth_temp_curves.df$species))

save(predictive.df, survey_sf, env_grid_sf, richness_df, prediction_all.df, margin_dist.df, coastline, depth_temp_curves.df, temp_depth_region_sub, temp_depth_sub, O2_temp_curves.df, temp_O2_region_sub, temp_O2_sub, RCP8.5_arrows, forecast_accuracy, coefs.df, file = "./data/outputs/SDM_projections_break.RData")
