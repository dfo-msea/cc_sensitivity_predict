---
title: "Groundfish biodiversity change in northeastern Pacific waters under projected warming and deoxygenation --- Appendix A"
subtitle: "Philosophical Transactions of the Royal Society B"
author: 
- Patrick L. Thompson, Jessica Nephin, Sarah Davies, Ashley Park, 
- Devin Lyons, Christopher N. Rooper, M. Angelica Peña, James Christian,
- Karen Hunter, Emily Rubidge, Amber M. Holdsworth
output:
    bookdown::pdf_document2:
      toc: false
      number_sections: false
      fig_caption: false
      keep_tex: false
csl: mee.csl
bibliography: Climate_sensitivity_refs.bib
always_allow_html: true
link-citations: yes
header-includes:
  \newcommand{\beginsupplement}{
  \setcounter{equation}{0}
  \renewcommand{\theequation}{S.\arabic{equation}}
  \setcounter{table}{0}  
  \renewcommand{\thetable}{S\arabic{table}} 
  \setcounter{figure}{0} 
  \renewcommand{\thefigure}{S\arabic{figure}}}
  \usepackage{pdflscape}
  \newcommand{\blandscape}{\begin{landscape}}
  \newcommand{\elandscape}{\end{landscape}}
  \extrafloats{100}
---
```{r, include=FALSE}
options(tinytex.verbose = TRUE)
```

```{r, echo=FALSE, include = FALSE}
# devtools::install_github("cboettig/knitcitations@v1")
library(knitcitations); cleanbib()
cite_options(citation_format = "pandoc", check.entries=FALSE)
library(bibtex)
library(knitr)
library(kableExtra)
```

```{r load_packages, include=FALSE}
#data manipulation
library(tidyverse)

#plotting
library(corrplot)
library(viridis)
library(RColorBrewer)
#library(cowplot)
library(sf)
library(PupillometryR)
library(scales)
library(patchwork)
```

```{r include = FALSE}
load("../data/outputs/SDM_projections_break.RData")

source("../code/functions/plt_plot_theme.R")
gf_names.df <- read.csv("../data/gf_names.csv")

survey_region <- survey_sf %>% st_crop(env_grid_sf)

theme_set(plt_theme)

reverselog_trans <- function(base = exp(1)) {
  trans <- function(x) -log(x, base)
  inv <- function(x) base^(-x)
  trans_new(paste0("reverselog-", format(base)), trans, inv,
            log_breaks(base = base),
            domain = c(1e-100, Inf))
}
knitr::opts_chunk$set(dev="png")
```

\beginsupplement
# Single species supplemental figures
The following text outlines the elements of the Supplemental Figures. Each figure corresponds to a single species, labeled at the top of the figure. 

**Panels a--d** show modelled (contour lines) and observed (coloured points) occurrences based on seafloor depth and temperature (a,c) or temperature and dissolved oxygen (b, d). Panels a and b show observed data from the entire coast, panels c and d show the subset of this data from the focal region where future projections were made. The coloured circles represent gridded values across this parameter space, with the size of each circle showing the number of corresponding trawls, and the colour showing the proportion of trawls where the species was present. The SDM modelled responses are shown as contour lines over this environmental space, with each line corresponding to a given probability of occurrence as indicated by its colour. These SDM modelled responses were fit on the entire coastwide dataset (a--b), but we include these contour lines in panels c and d to show how these SDM modelled fits correspond to the environmental space and observations in the focal region. The model response curves for seafloor depth and temperature are produced by making predictions using the fitted SDM model across a regular grid (30 x 30) of environmental conditions from the minimum to the maximum depth (log) and temperature values in the coastwide dataset, excluding combinations that fell outside of the observed environmental space. Dissolved oxygen for each depth, temperature combination was obtained by binning trawls within this grid and calculating the median value. The model response curves for seafloor temperature and oxygen (b, d) were produced in the same way, but with depth instead of oxygen estimated from the binned data. 

**Panels e--h** shows projected changes in occurrence between 1986--2005 and 2046--2065. These are based on applying the fitted SDM model to seafloor depth, temperature, and dissolved oxygen at a 3 km resolution from a hindcast (1986--2005) and future projection (2046--2065) of the BCCM [e, g\; @pena2019] and the NEP36 model [f, h\; @holdsworth2021]. Panels e and f show projections based on the RCP 4.5 emissions scenario, panels g and h show projections based on the RCP 8.5 emissions scenario. The map shows the difference between the hindcast and future projected mean occurrences, with blue shades indicating decreased occurrence and red and orange shades indicating increases in occurrence. We assume that areas with an occurrence probability of 0.1 or greater are suitable habitat. Grid cells where both the hindcast and the future projection fall below this threshold are coloured grey. The inset histogram serves as a colour legend and shows the distribution of projected occurrence change values across all 3 km grid cells in the focal region. 

Visit our Shiny app for an interactive version of these figures -- https://msea.science/gf_climate_shiny/.

\newpage
\blandscape

```{r specPlots, fig.align="center", fig.height = 13, fig.width = 16, echo=FALSE, warning = FALSE, results='hide', message=FALSE}
for(i in unique(depth_temp_curves.df$species)[order(unique(depth_temp_curves.df$species))]){
  
  sci_name <- gf_names.df$science_name[gf_names.df$species_common_name == i]
  
  p1 <- depth_temp_curves.df %>%
  filter(species == i) %>% 
  ggplot(aes(y = depth, x = temperature_C))+
  scale_y_continuous(trans = reverselog_trans(10), limits =  c(1430, 10), breaks = c(seq(10,100, by = 10), seq(200,1000, by = 100)), labels = c("10", rep("", 8), 100, rep("", 8), 1000))+
  scale_x_continuous(breaks = seq(-1,15, by = 1), limits = c(-1.4,14.55), labels = c("", "1", rep("", 4), 5, rep("", 4), 10, rep("",4) ,15))+
  geom_point(data = temp_depth_sub  %>% filter(species == i), aes(x = temp_bin, y= depth_bin, fill = prop, size = total_trawls), pch = 21)+
  scale_fill_viridis_c(name = "prop. of tows", end = 1, guide = "none", limits = c(0,1))+
  geom_contour(aes(z = predict, color = factor(after_stat(level), levels = seq(0.1, 0.9, by = 0.2), ordered = TRUE)), breaks = seq(0.1,0.9, by = 0.2), size = 1)+
  scale_color_viridis_d(name = "prob.\noccurence", begin = 0.1, end = 0.9, drop = FALSE, guide = "none")+
  scale_size_continuous(trans = "sqrt", guide = "none", range = c(0.5, 5))+
  ylab("Depth (m)")+
  xlab(expression("Temperature " ( degree~C)))+
  theme(legend.position = c(1, 0), legend.justification = c(1,0))+
  theme(legend.box = "horizontal")+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  ggtitle("Entire coast")


p2 <- O2_temp_curves.df %>%
  filter(species == i) %>%
  ggplot(aes(y = O2, x = temperature_C))+
   scale_y_log10(limits =  c(0.045, 12.6), breaks = c(seq(0.04, 0.09, by = 0.01), seq(0.1,1, by = 0.1), seq(2,10, by = 1)), labels = c(rep("", 6), 0.1, rep("", 8), 1, rep("", 8), 10))+
  scale_x_continuous(breaks = seq(-1,15, by = 1), limits = c(-1.4,14.55), labels = c("", "1", rep("", 4), 5, rep("", 4), 10, rep("",4) ,15))+
  geom_point(data = temp_O2_sub %>% filter(species == i), aes(x = temp_bin, y= O2_bin, fill = prop, size = total_trawls), pch = 21)+
  scale_fill_viridis_c(name = "prop.\nof tows", end = 1, limits = c(0,1), guide = "none")+
  geom_contour(aes(z = predict, color = factor(after_stat(level), levels = seq(0.1, 0.9, by = 0.2), ordered = TRUE)), breaks = seq(0.1,0.9, by = 0.2), size = 1)+
  scale_color_viridis_d(name = "prob.\noccurence", begin = 0.1, end = 0.9, drop = FALSE, guide = "none")+
  scale_size_continuous(trans = "sqrt", guide = "none", range = c(0.5, 5))+
  ylab("Dissolved oxygen (ml/L)")+
  xlab(expression("Temperature " ( degree~C)))+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  ggtitle("Entire coast")


p3 <- depth_temp_curves.df %>% 
  filter(species == i) %>%
  ggplot(aes(y = depth, x = temperature_C))+
   scale_y_continuous(trans = reverselog_trans(10), limits =  c(1430, 10), breaks = c(seq(10,100, by = 10), seq(200,1000, by = 100)), labels = c("10", rep("", 8), 100, rep("", 8), 1000))+
  scale_x_continuous(breaks = seq(-1,15, by = 1), limits = c(-1.4,14.55), labels = c("", "1", rep("", 4), 5, rep("", 4), 10, rep("",4) ,15))+
  geom_point(data = temp_depth_region_sub %>% filter(species == i), aes(x = temp_bin, y= depth_bin, fill = prop, size = total_trawls), pch = 21)+
  scale_fill_viridis_c(name = "prop. of tows", end = 1, limits = c(0,1))+
  geom_contour(aes(z = predict, color = factor(after_stat(level), levels = seq(0.1, 0.9, by = 0.2), ordered = TRUE)), breaks = seq(0.1,0.9, by = 0.2), size = 1)+
  scale_color_viridis_d(name = "prob.\noccurence", begin = 0.1, end = 0.9, drop = FALSE)+
  scale_size_continuous(trans = "sqrt", range = c(0.5, 5), guide = "none")+
  ylab("Depth (m)")+
  xlab(expression("Temperature " ( degree~C)))+
  theme(legend.position = "bottom")+
  theme(legend.box = "vertical")+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  geom_segment(data = RCP8.5_arrows %>% filter(species == i), aes(x = temp_initial, y = depth_m, xend = temperature, yend = depth_m),
               arrow = arrow(length = unit(0.10, "cm")), size = 0.75, lineend = "butt", linejoin = "mitre", color = "red")+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  ggtitle("Focal region")


p4 <- O2_temp_curves.df %>%
  filter(species == i) %>%
  ggplot(aes(y = O2, x = temperature_C))+
   scale_y_log10(limits =  c(0.045, 12.6), breaks = c(seq(0.04, 0.09, by = 0.01), seq(0.1,1, by = 0.1), seq(2,10, by = 1)), labels = c(rep("", 6), 0.1, rep("", 8), 1, rep("", 8), 10))+
  scale_x_continuous(breaks = seq(-1,15, by = 1), limits = c(-1.4,14.55), labels = c("", "1", rep("", 4), 5, rep("", 4), 10, rep("",4) ,15))+
  geom_point(data = temp_O2_region_sub %>% filter(species == i), aes(x = temp_bin, y= O2_bin, fill = prop, size = total_trawls), pch = 21)+
  scale_fill_viridis_c(name = "prop.\nof tows", end = 1, limits = c(0,1), guide = "none")+
  geom_contour(aes(z = predict, color = factor(after_stat(level), levels = seq(0.1, 0.9, by = 0.2), ordered = TRUE)), breaks = seq(0.1,0.9, by = 0.2), size = 1)+
  scale_color_viridis_d(name = "prob.\noccurence", begin = 0.1, end = 0.9, drop = FALSE, guide = "none")+
  scale_size_continuous(trans = "sqrt", range = c(0.5, 5))+
  ylab("Dissolved oxygen (ml/L)")+
  xlab(expression("Temperature " ( degree~C)))+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  geom_segment(data = RCP8.5_arrows  %>% filter(species == i), aes(x = temp_initial, y = O2_initial, xend = temperature, yend = O2),
               arrow = arrow(length = unit(0.10, "cm")), size = 0.75, lineend = "butt", linejoin = "mitre", color = "red")+
  theme(legend.box = "horizontal")+
  theme(legend.title = element_text(size = 9),
        legend.text = element_text(size = 7))+
  theme(legend.position = "bottom")+
  ggtitle("Focal region")

ocur_hist4.5 <- prediction_all.df %>% 
  filter(scenario == "RCP.4.5", species == i, model == "BCCM") %>% 
  ggplot(aes(x = diff_in_range , fill = ..x..))+
  geom_path(data = data.frame(diff_in_range = seq(-0.816, 0.652, length = 100)), aes(y = -300, color = ..x..), size = 1)+
  geom_histogram(bins = 30, color = "grey50", size = 0.1) +
  geom_vline(xintercept = 0, linetype = 2)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  xlab("change in occurrence")+
  scale_x_continuous(limits = c(-0.816, 0.652))+
    scale_y_continuous(limits = c(-300, 6000))+
  theme(axis.line.y = element_blank(),
        axis.line.x = element_line(),
        axis.text.x = element_text(size = 8),
        axis.title.x = element_text(size = 10),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        axis.title.y=element_blank(),
        legend.position="none",
        panel.background=element_blank(),
        panel.border=element_blank(),
        #panel.grid.major=element_blank(),
        panel.grid.minor=element_blank(),
        plot.background=element_blank())

ocur_hist <- prediction_all.df %>% 
  filter(scenario == "RCP.8.5", species == i, model == "BCCM") %>% 
  ggplot(aes(x = diff_in_range , fill = ..x..))+
  geom_path(data = data.frame(diff_in_range = seq(-0.816, 0.652, length = 100)), aes(y = -300, color = ..x..), size = 1)+
  geom_histogram(bins = 30, color = "grey50", size = 0.1) +
  geom_vline(xintercept = 0, linetype = 2)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  xlab("change in occurrence")+
  scale_x_continuous(limits = c(-0.816, 0.652))+
  scale_y_continuous(limits = c(-300, 6000))+
  theme(axis.line.y = element_blank(),
        axis.line.x = element_line(),
        axis.text.x = element_text(size = 8),
        axis.title.x = element_text(size = 10),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        axis.title.y=element_blank(),
        legend.position="none",
        panel.background=element_blank(),
        panel.border=element_blank(),
        #panel.grid.major=element_blank(),
        panel.grid.minor=element_blank(),
        plot.background=element_blank())

ocur_histNEP36.8.5 <- prediction_all.df %>% 
  filter(scenario == "RCP.8.5", species == i, model == "NEP36") %>% 
  ggplot(aes(x = diff_in_range , fill = ..x..))+
  geom_path(data = data.frame(diff_in_range = seq(-0.816, 0.652, length = 100)), aes(y = -300, color = ..x..), size = 1)+
  geom_histogram(bins = 30, color = "grey50", size = 0.1) +
  geom_vline(xintercept = 0, linetype = 2)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  xlab("change in occurrence")+
  scale_x_continuous(limits = c(-0.816, 0.652))+
    scale_y_continuous(limits = c(-300, 6000))+
  theme(axis.line.y = element_blank(),
        axis.line.x = element_line(),
        axis.text.x = element_text(size = 8),
        axis.title.x = element_text(size = 10),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        axis.title.y=element_blank(),
        legend.position="none",
        panel.background=element_blank(),
        panel.border=element_blank(),
        #panel.grid.major=element_blank(),
        panel.grid.minor=element_blank(),
        plot.background=element_blank())

ocur_histNEP36.4.5 <- prediction_all.df %>% 
  filter(scenario == "RCP.4.5", species == i, model == "NEP36") %>% 
  ggplot(aes(x = diff_in_range , fill = ..x..))+
  geom_path(data = data.frame(diff_in_range = seq(-0.816, 0.652, length = 100)), aes(y = -300, color = ..x..), size = 1)+
  geom_histogram(bins = 30, color = "grey50", size = 0.1) +
  geom_vline(xintercept = 0, linetype = 2)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  xlab("change in occurrence")+
  scale_x_continuous(limits = c(-0.816, 0.652))+
    scale_y_continuous(limits = c(-300, 6000))+
  theme(axis.line.y = element_blank(),
        axis.line.x = element_line(),
        axis.text.x = element_text(size = 8),
        axis.title.x = element_text(size = 10),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        axis.title.y=element_blank(),
        legend.position="none",
        panel.background=element_blank(),
        panel.border=element_blank(),
        #panel.grid.major=element_blank(),
        panel.grid.minor=element_blank(),
        plot.background=element_blank())

diff_map4.5 <- prediction_all.df %>% 
  filter(scenario == "RCP.4.5", species == i, model == "BCCM") %>% 
  ggplot()+
  geom_tile(aes(x = x, y = y, fill = diff_in_range, color = diff_in_range), size = 0.5)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  ggtitle("Predicted occurrence change")+
  geom_sf(data = coastline, fill = "grey97", color = "grey10")+
  theme(
    legend.position = c(0, 0), legend.justification = c(0,0))+
  annotation_custom(ggplotGrob(ocur_hist4.5), xmin = 500000, xmax = 900000, ymin = 100000, ymax = 400000)+
  ggtitle("Projected occurrence change\n(2046-2065, BCCM, RCP 4.5)")+
  ylab("")+
  xlab("")+
  theme_bw()

diff_map8.5NEP36 <- prediction_all.df %>% 
  filter(scenario == "RCP.8.5", species == i, model == "NEP36") %>% 
  ggplot()+
  geom_tile(aes(x = x, y = y, fill = diff_in_range, color = diff_in_range), size = 0.5)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  ggtitle("Predicted occurrence change")+
  geom_sf(data = coastline, fill = "grey97", color = "grey10")+
  theme(legend.position = c(0, 0), legend.justification = c(0,0))+
  annotation_custom(ggplotGrob(ocur_histNEP36.8.5), xmin = 500000, xmax = 900000, ymin = 100000, ymax = 400000)+
  ggtitle("Projected occurrence change\n(2046-2065, NEP36, RCP 8.5)")+
  ylab("")+
  xlab("")+
  theme_bw()

diff_map4.5NEP36 <- prediction_all.df %>% 
  filter(scenario == "RCP.4.5", species == i, model == "NEP36") %>% 
  ggplot()+
  geom_tile(aes(x = x, y = y, fill = diff_in_range, color = diff_in_range), size = 0.5)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  ggtitle("Predicted occurrence change")+
  geom_sf(data = coastline, fill = "grey97", color = "grey10")+
  theme(
    legend.position = c(0, 0), legend.justification = c(0,0))+
  annotation_custom(ggplotGrob(ocur_histNEP36.4.5), xmin = 500000, xmax = 900000, ymin = 100000, ymax = 400000)+
  ggtitle("Projected occurrence change\n(2046-2065, NEP36, RCP 4.5)")+
  ylab("")+
  xlab("")+
  theme_bw()

diff_map <- prediction_all.df %>% 
  filter(scenario == "RCP.8.5", species == i, model == "BCCM") %>% 
  ggplot()+
  geom_tile(aes(x = x, y = y, fill = diff_in_range, color = diff_in_range), size = 0.5)+
  scale_fill_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  scale_color_distiller(palette = "RdYlBu", guide = "none", limits = c(-0.816, 0.652), direction = -1)+
  ggtitle("Predicted occurrence change")+
  geom_sf(data = coastline, fill = "grey97", color = "grey10")+
  theme(legend.position = c(0, 0), legend.justification = c(0,0))+
  annotation_custom(ggplotGrob(ocur_hist), xmin = 500000, xmax = 900000, ymin = 100000, ymax = 400000)+
  ggtitle("Projected occurrence change\n(2046-2065, BCCM, RCP 8.5)")+
  ylab("")+
  xlab("")+
  theme_bw()

# range_map <-  prediction_all.df %>% 
#   filter(scenario == "RCP.8.5", species == i, model == "BCCM") %>%
#   ggplot()+
#   geom_tile(aes(x = X, y = y, fill = range_change, color = range_change), size = 0.5)+
#   scale_fill_manual(values = c("grey50", "#dd1c77", "#31688EFF", "#35B779FF"), name = "", drop = FALSE)+
#   scale_color_manual(values = c("grey50", "#dd1c77", "#31688EFF", "#35B779FF"), name = "", drop = FALSE)+
#   ggtitle("Predicted range change")+
#   geom_sf(data = coastline, fill = "grey97", color = "grey10")+
#   theme(legend.position = c(0, 0), legend.justification = c(0,0))+
#   guides(color = guide_legend(override.aes = list(size = 2)))+
#   ggtitle("Projected suitability\n(2046-2065 - RCP 8.5)")+
#   ylab("")+
#   xlab("")+
#   theme_bw()


species_plot  <- (p1 + p2 + p3 + p4) | (diff_map4.5 + diff_map4.5NEP36 + diff_map + diff_map8.5NEP36)
full_plot <- species_plot + plot_annotation(tag_levels = 'a', title = i , subtitle = sci_name, theme = theme(plot.title = element_text(size = 24, hjust = 0), plot.subtitle = element_text(size = 20, hjust = 0, face = "italic")))
print(full_plot)
}

```

\elandscape
\newpage
# References
<div id="refs"></div>
